function xd() {
	var letra = document.getElementById("a").value;
	switch (letra) {
		case 'a':
			sonarBombo();
			document.getElementById("a").value = '';
			break;
		case 's':
			sonartomaereo();
			document.getElementById("a").value = '';
			break;
		case 'd':
			sonarplatilloiz();
			document.getElementById("a").value = '';
			break;
		case 'w':
			sonarplatilloride();
			document.getElementById("a").value = '';
			break;
		case 'e':
			sonarhit();
			document.getElementById("a").value = '';
			break;
		case 'f':
			sonarcaja();
			document.getElementById("a").value = '';
			break;
		case 'q':
			sonartomsuelo();
			document.getElementById("a").value = '';
			break;
		case 'z':
			sonarplatilloder();
			document.getElementById("a").value = '';
			break;
	}
}
// Bombo
window.addEventListener("load", function () {
	document.getElementById("bombo").addEventListener("click", sonarBombo);
});
function sonarBombo() {
	var sonido = document.createElement("iframe");
	sonido.setAttribute("src", "sounds/bombo.mp3");
	document.body.appendChild(sonido);
	document.getElementById("bombo").addEventListener("click", sonarBombo);
}

// Tom aereo
window.addEventListener("load", function () {
	document.getElementById("tom-aereo").addEventListener("click", sonartomaereo);
});
function sonartomaereo() {
	var sonido = document.createElement("iframe");
	sonido.setAttribute("src", "sounds/tom-aereo.mp3");
	document.body.appendChild(sonido);
	document.getElementById("tom-aereo").addEventListener("click", sonartomaereo);
}

// Platillo izquierdo
window.addEventListener("load", function () {
	document.getElementById("platillo-iz").addEventListener("click", sonarplatilloiz);
});
function sonarplatilloiz() {
	var sonido = document.createElement("iframe");
	sonido.setAttribute("src", "sounds/platillo-crash-izquierdo.mp3");
	document.body.appendChild(sonido);
	document.getElementById("platillo-iz").addEventListener("click", sonarplatilloiz);
}

//Platillo ride
window.addEventListener("load", function () {
	document.getElementById("platillo-ride").addEventListener("click", sonarplatilloride);
});
function sonarplatilloride() {
	var sonido = document.createElement("iframe");
	sonido.setAttribute("src", "sounds/platillo-ride.mp3");
	document.body.appendChild(sonido);
	document.getElementById("platillo-ride").addEventListener("click", sonarplatilloride);
}

//Hit Hat
window.addEventListener("load", function () {
	document.getElementById("hit").addEventListener("click", sonarhit);
});
function sonarhit() {
	var sonido = document.createElement("iframe");
	sonido.setAttribute("src", "sounds/hit-hat.mp3");
	document.body.appendChild(sonido);
	document.getElementById("hit").addEventListener("click", sonarhit);
}

// Caja
window.addEventListener("load", function () {
	document.getElementById("caja").addEventListener("click", sonarcaja);
});
function sonarcaja() {
	var sonido = document.createElement("iframe");
	sonido.setAttribute("src", "sounds/caja.mp3");
	document.body.appendChild(sonido);
	document.getElementById("caja").addEventListener("click", sonarcaja);
}

// Tom suelo
window.addEventListener("load", function () {
	document.getElementById("tom-suelo").addEventListener("click", sonartomsuelo);
});
function sonartomsuelo() {
	var sonido = document.createElement("iframe");
	sonido.setAttribute("src", "sounds/tom-suelo.mp3");
	document.body.appendChild(sonido);
	document.getElementById("tom-suelo").addEventListener("click", sonartomsuelo);
}

//Platillo Derecho
window.addEventListener("load", function () {
	document.getElementById("platillo-der").addEventListener("click", sonarplatilloder);
});
function sonarplatilloder() {
	var sonido = document.createElement("iframe");
	sonido.setAttribute("src", "sounds/platillo-crash-derecho2.mp3");
	document.body.appendChild(sonido);
	document.getElementById("platillo-der").addEventListener("click", sonarplatilloder);
}

